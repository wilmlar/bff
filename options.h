/*
bff
Copyright (C) 2021  William Larsson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef OPTIONS_H
#define OPTIONS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <argp.h>

char *prog_name;
#define MAX_MEM    1000000

// Program description
static char doc[] = "A brainfuck interpreter for the modern world";

// Accepted arguments
static char args_doc[] = "[FILE]";

// COMMAND LINE OPTIONS
static struct argp_option options[] = {
  {"wrap",          'w', 0,       0, "Enable memory wrap" },
  {"instructions",  'i', 0,       0, "Print a list of compiled instructions" },
  {"script",        's', 0,       0, "Print a cleaned brainfuck script without comments" },
  {"memory",        'm', "N",     0, "Set memory size to N" },
  {"eof",           'e', "VALUE", 0, "Choose which character to store when EOF is read" },
  { 0 }
};

// COMMAND LINE ARGUMENTS
struct arguments  {
  char *filepath; // File to be read
  int wrap;       // Enable memory wrap
  int instr;      // Print instructions
  int script;     // Print script
  int memSize;    // Set memory size
  char *eof;      // Set EOF behavior
};

// FUNCTION DECLARATIONS

int getOptions (int key, char *arg, struct argp_state *state);

#endif
