/*
bff
Copyright (C) 2021  William Larsson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "bff.h"

/************************************************************************/
// Make an instruction token from a series of characters
instruction makeInstruction(char ch, unsigned int value, unsigned int addr)  {
  instruction instr;
  instr.ch = ch;
  instr.value = value;
  instr.addr = addr;
  switch (ch) {
    case '+': instr.type = "ADD";    break;
    case '-': instr.type = "SUB";    break;
    case '<': instr.type = "LEFT";   break;
    case '>': instr.type = "RIGHT";  break;
    case '.': instr.type = "WRITE";  break;
    case ',': instr.type = "READ";   break;
    case '[': instr.type = "SKIP";   break;
    case ']': instr.type = "JUMP";   break;
  }
  return instr;
}

/************************************************************************/
// Compile brainfuck script to an array with instruction tokens
instruction *compile(FILE *script) {
  instruction *program = malloc(sizeof(instruction)*PROG_SIZE);

  unsigned short stack[STACK_SIZE]; // Jump stack
  unsigned short *sp = stack;       // Stack pointer
  unsigned int addr = 0;            // Program address

  char ch = fgetc(script);
  char lastChar;
  while(ch != EOF)  {
    // Get legal characters
    if(LEGAL(ch)) {
      lastChar = ch;
      unsigned int val = 1;
      // Basic operations "+-<>"
      if(strchr("+-<>", ch)) {
        while((ch = fgetc(script)) == lastChar || ILLEGAL(ch))  {
          if(ch == lastChar)  { val++; }
        }
      }
      // Input/Output and conditionals ".,[]"
      else if(strchr(".,[]", ch)) {
        switch(ch)  {
          case '[':
            if(sp >= stack + STACK_SIZE)  {
              fprintf(stderr, "%s: Stack error: Stack overflow\n", prog_name);
              exit(EXIT_FAILURE);
            }
            // Push to stack
            ++sp;
            *sp = addr;
            break;
          case ']':
            if(sp <= stack)  {
              fprintf(stderr, "%s: Stack error: Missing open bracket\n", prog_name);
              exit(EXIT_FAILURE);
            }
            // Pop from stack
            val = *sp;
            program[val].value = addr;
            --sp;
            break;
          case '.':
          case ',':
            break;
        }
        // Get next character
        ch = fgetc(script);
      }
      // Add instruction to program
      program[addr] = makeInstruction(lastChar, val, addr);
      addr++;
      if(addr >= PROG_SIZE) {
        fprintf(stderr, "%s: Can't access instruction '%d': Max program memory exceded\n", prog_name, addr);
        exit(EXIT_FAILURE);
      }
    }
    // Ignore illegal charaters
    else  {
      while(ILLEGAL(ch))  {
        ch = fgetc(script);
      }
    }
  }
  if(sp > stack) {
    fprintf(stderr, "%s: Stack error: Missing closed bracket\n", prog_name);
    exit(EXIT_FAILURE);
  }
  return program;
}

/************************************************************************/
// Execute an array of instructions
void execute(instruction *prog, int memSize, int wrap, char *eofValue) {
  unsigned int pc = 0;              // Program counter
  instruction instr = prog[pc];     // 1st instruction
  char memory[memSize];          // Memory cells
  memset(memory, 0, memSize);
  char *cell = memory;              // Current cell
  char inputChar;


  while(instr.ch) {
    switch (instr.ch) {
      // Add to cell
      case '+': *cell += instr.value;  break;
      // Subtract from cell
      case '-': *cell -= instr.value;  break;
      // Move to previous cell
      case '<':
        cell -= instr.value;
        if(cell < memory)  {
          if(wrap)  {  cell += memSize;  }
          else  {
            fprintf(stderr, "%s: Can't access memory address '%ld': Adress out of bounds\n", prog_name, cell - memory);
            exit(EXIT_FAILURE);
          }
        }
        break;
      // Move to next cell
      case '>':
        cell += instr.value;
        if(cell >= memory + memSize)  {
          if(wrap)  {  cell -= memSize;  }
          else  {
            fprintf(stderr, "%s: Can't access memory address '%ld': Adress out of bounds\n", prog_name, cell - memory);
            exit(EXIT_FAILURE);
          }
        }
        break;
      // Print character
      case '.':  putchar(*cell);       break;
      // Read from stdin
      case ',':
        inputChar = getchar();
        if (inputChar == EOF) {
          if(!strcmp(eofValue, "none")) {
            break;
          }
          else if(!strcmp(eofValue, "eof")) {
            inputChar = -1;
          }
          else if(!strcmp(eofValue, "zero")) {
            inputChar = 0;
          }
        }
        *cell = inputChar;
        break;
      // Skip if zero
      case '[':
        if(*cell == 0)  {
          pc = instr.value;
        }
        break;
      // Return if not zero
      case ']':
        if(*cell != 0)  {
          pc = instr.value;
        }
        break;
    }
    pc++;              // Advance the program counter
    instr = prog[pc];  // Get next instruction
  }
}

/************************************************************************/
// Print a cleaned brainfuck script to stdout
void showScript(instruction *program)  {
  instruction instr;
  for(int i=0; program[i].ch; i++)  {
    instr = program[i];
    if(strchr("+-<>.,", instr.ch))  {
      for(int n=0; n<instr.value; n++) {
        putchar(instr.ch);
      }
    }
    else  { putchar(instr.ch); }
  }
  putchar('\n');
}

/************************************************************************/
// Print instruction tokens to stdout
void showInstructions(instruction *program)  {
  instruction instr;
  for(int i=0; program[i].ch; i++) {
    instr = program[i];
    if(strchr(".,", instr.ch)) {
      printf("%d: %s\n", instr.addr, instr.type);
    }
    else  {
      printf("%d: %s: %d\n", instr.addr, instr.type, instr.value);
    }
  }
}

/************************************************************************/
