/*
bff
Copyright (C) 2021  William Larsson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef BFF_H
#define BFF_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char *prog_name;

// MACROS
#define LEGAL(ch)(strchr("+-<>.,[]", ch))
#define ILLEGAL(ch)(!strchr("+-<>.,[]", ch) && ch != EOF)
#define PROG_SIZE  0xffff
#define STACK_SIZE 0xffff

// INSTRUCTION TOKEN
struct Instruction  {
  char ch;
  char *type;
  unsigned int addr;
  unsigned int value;
};
typedef struct Instruction instruction;

// FUNCTION DECLARATIONS
instruction makeInstruction(char ch, unsigned int value, unsigned int addr);
instruction *compile(FILE *script);
void execute(instruction *prog, int memSize, int wrap, char *eofValue);
void showScript(instruction *program);
void showInstructions(instruction *program);

#endif
