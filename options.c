/*
bff
Copyright (C) 2021  William Larsson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include "options.h"

const char *argp_program_version = "bff 1.0.0";

// Parse command line arguments
int getOptions (int key, char *arg, struct argp_state *state)  {
  struct arguments *arguments = state->input;
  switch (key)  {
    case 'w':
      arguments->wrap = 1;
      break;
    case 'i':
      arguments->instr = 1;
      break;
    case 's':
      arguments->script = 1;
      break;
    case 'm':
      arguments->memSize = atoi(arg);
      if(arguments->memSize == 0) {
        fprintf(stderr, "%s: Invalid memory option '%s': Expected a non zero number\n", prog_name, arg);
        exit(EXIT_FAILURE);
      }
      if(arguments->memSize >= MAX_MEM) {
        fprintf(stderr, "%s: Invalid memory option '%s': Max memory of %d cells exceded\n", prog_name, arg, MAX_MEM);
        exit(EXIT_FAILURE);
      }
      break;
    case 'e':
      if (!strcmp(arg, "eof") || !strcmp(arg, "zero") || !strcmp(arg, "none")) {
        arguments->eof = arg;
      }
      else  {
        fprintf(stderr, "%s: Invalid argument '%s': Unrecognized EOF behavior\n", prog_name, arg);
        exit(EXIT_FAILURE);
      }
      break;

    case ARGP_KEY_ARG:
      // Too many arguments
      if (state->arg_num >= 1)  {
        fprintf(stderr, "%s: Too many arguments: Can't open more than one file\n", prog_name);
        exit(EXIT_FAILURE);
      }
      // Read from file
      arguments->filepath = arg;
      break;

    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}
