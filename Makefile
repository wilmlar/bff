TARGET := bff

all: $(TARGET)

$(TARGET): main.o options.o bff.o
	$(CC) main.o options.o bff.o -o $(TARGET)

main.o: main.c
	$(CC) -c main.c

options.o: options.c options.h
	$(CC) -c options.c

bff.o: bff.c bff.h
	$(CC) -c bff.c

clean:
	rm *.o $(TARGET)

install: $(TARGET)
	cp $(TARGET) /usr/bin
	chmod 755 /usr/bin/$(TARGET)

uninstall:
	rm /usr/bin/$(TARGET)
