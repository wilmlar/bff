/*
bff
Copyright (C) 2021  William Larsson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <stdio.h>
#include "options.h"
#include "bff.h"

// MAIN
int main (int argc, char **argv)  {
  prog_name = argv[0];
  static struct argp argp = { options, getOptions, args_doc, doc }; // Options parser
  struct arguments arguments; // Command line arguments

  // Argument default values
  arguments.wrap = 0;
  arguments.instr = 0;
  arguments.script = 0;
  arguments.memSize = 32000;
  arguments.eof = "eof";

  // Parse arguments
  argp_parse (&argp, argc, argv, 0, 0, &arguments);

  instruction *program;
  // Read from file
  if (strcmp(arguments.filepath, "")) {
    FILE *f = fopen(arguments.filepath, "r+");
    if(errno) {
      fprintf(stderr, "%s: Can't open file '%s': %s\n", prog_name, arguments.filepath, strerror(errno));
      exit(EXIT_FAILURE);
    }
    program = compile(f);
    fclose(f);
  }
  // Read from stdin
  else  {
    program = compile(stdin);
  }

  // Execute program
  if      (arguments.script) {  showScript(program);        }
  else if (arguments.instr)  {  showInstructions(program);  }
  else                       {  execute(program, arguments.memSize, arguments.wrap, arguments.eof);           }

  return 0;
}
